use std::io::BufRead;
use std::collections::HashMap;

fn main() {
    let stdin = std::io::stdin();
    let colors: HashMap<char, &str> = [
    ('0', "38;5;8"),
    ('1', "31"),
    ('2', "32"),
    ('3', "34"),
    ('4', "33"),
    ('5', "38;5;202"),
    ('6', "36"),
    ('7', "38;5;94"),
    ('8', "35"),
    ('9', "37"),
    ('#', "31;1"),
    ('.', "30;1"),
    ('@', "33;1"),
        ].iter().cloned().collect();
    for line in stdin.lock().lines() {
        let line = line.expect("Couldn't read");
        for c in line.chars() {
            match colors.get(&c) {
                Some(s) => print!("\u{001b}[{}m{}\u{001b}[0m", s, c),
                None => print!("{}", c),
            }
        }
        println!("");
    }
}
