#include <bits/stdc++.h>
using namespace std;

int main() {
	string s;
	while(getline(cin, s)) {
		string out;
		for(char c : s) if('0' > c || c > '9') out.push_back(c); else {
			out.append("\u001b[3");
			out.push_back('0' + (c - '0') % 8);
			out.append(";1m");
			out.push_back(c);
			out.append("\u001b[0m");
		}
		cout << out << endl;
	}
}
